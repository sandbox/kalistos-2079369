(function ($) {

Drupal.behaviors.secretKey = {
  attach: function (context) {
    $('.field-type-secret-key .form-submit', context).click(function() {
      $(this).prev('.form-text').val(generateKey(32));
      return false;
    });

    function generateKey(length) {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for (var i=0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      }

      return text;
    }
  }
};

})(jQuery);
