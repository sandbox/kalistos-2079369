<?php

/**
 * Implements hook_field_info().
 */
function secret_key_field_info() {
  // Init output variable.
  $items = array();

  $items['secret_key'] = array(
    'label' => t('Secret key'),
    'description' => t('This field stores secret key for any entity.'),
    'default_widget' => 'secret_key_text',
    'default_formatter' => 'secret_key_text',
  );

  return $items;
}

/**
 * Implements hook_field_is_empty().
 */
function secret_key_field_is_empty($item, $field) {
  return empty($item['secret_key']['value']);
}

/**
 * Implements hook_field_formatter_info().
 */
function secret_key_field_formatter_info() {
  // Init output variable.
  $items = array();

  $items['secret_key_text'] = array(
    'label' => t('Secret key'),
    'field types' => array('secret_key'),
  );

  return $items;
}

/**
 * Implements hook_field_formatter_view().
 */
function secret_key_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  // Init output variable.
  $element = array();

  switch ($display['type']) {
    case 'secret_key_text':
      foreach ($items as $delta => $item) {
        $element[$delta] = array('#markup' => check_plain($item['value']));
      }
      break;
  }

  return $element;
}

/**
 * Implements hook_field_widget_info().
 */
function secret_key_field_widget_info() {
  // Init output variable.
  $items = array();

  $items['secret_key_text'] = array(
    'label' => t('Secret key generator'),
    'field types' => array('secret_key'),
  );

  return $items;
}

/**
 * Implements hook_field_widget_form().
 */
function secret_key_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $value = isset($items[$delta]['value']) ? $items[$delta]['value'] : '';

  switch ($instance['widget']['type']) {
    case 'secret_key_text':
      $element['#attached']['js'][] = drupal_get_path('module', 'secret_key') . '/includes/js/secret_key.js';

      $element['secret_key'] = array(
        '#type' => 'item',
        '#title' => $element['#title'],
        '#required' => $element['#required'],
        '#description' => $element['#description'],
      );
      $element['secret_key']['value'] = array(
        '#title' => $element['#title'],
        '#theme_wrappers' => array(),
        '#required' => $element['#required'],
        '#delta' => $element['#delta'],
        '#type' => 'textfield',
        '#default_value' => $value,
        '#maxlength' => 32,
      );
      $element['secret_key']['generator'] = array(
        '#type' => 'button',
        '#value' => t('Generate key'),
      );

      break;
  }

  return $element;
}

/**
 * Implements hook_field_presave().
 */
function secret_key_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  if ($field['type'] === 'secret_key') {
    foreach ($items as $delta => $item) {
      if (isset($item['secret_key']['value'])) {
        $items[$delta]['value'] = $item['secret_key']['value'];
        unset($items[$delta]['secret_key']);
      }
    }
  }
}
